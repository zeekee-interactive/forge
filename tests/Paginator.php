<?php

require_once __DIR__ . '/../Paginator.php';

// Creates an array with 31 elements, 0 -> 30
$set = range(0, 30);

// Initialize the object to get the total number of pages
$pages = new Paginator($set);

// Until we've hit all the pages, display the results for the current page
for($i = 1; $i <= $pages->total; $i++) {
  $object = new Paginator($set, $i);

  $prev = $object->previous();

  if($prev) {
    print 'Previous page: ' . $prev . "\n";
  }

  print 'Current page: ' . $i . "\n";

  $next = $object->next();

  if($next) {
    print 'Next page: ' . $next . "\n";
  }

  var_dump($object->results);

  print "\n ---- \n\n";
}