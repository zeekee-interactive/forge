<?php

class Paginator
{
  /**
   * Designates how many products to show per page.
   *
   * @access private
   * @var int
   */
  private $limit;


  /**
   * Offset of the current page.
   *
   * @access private
   * @var int
   */
  private $offset;


  /**
   * Information to be shown across all pages.
   *
   * @access public
   * @var array
   */
  public $set;


  /**
   * Information only to be shown on the current page.
   *
   * @access public
   * @var array
   */
  public $results;


  /**
   * The total number of pages for the resultset.
   *
   * @access public
   * @var int
   */
  public $total;


  /**
   * Sets properties for the object and calls Paginate::getResultsAtOffset().
   *
   * @param $set array The resultset to be paginated.
   * @param $offset int Current page being accessed (default is 1).
   * @param $limit int The maximum amount of items to display on the page (default is 10).
   * @throws Exception if the offset is out of bounds.
   * @see Paginate::getResultsAtOffset()
   * @return void
   */
  function __construct($set, $offset = 1, $limit = 10)
  {
    $this->set    = array_values($set);
    $this->offset = $offset;
    $this->limit  = 10;

    $this->total = $this->getTotalPages();

    if($this->total < $this->offset) {
      throw new Exception('No results could be found for your query.');
    }

    $this->results = $this->getResultsAtOffset();
  }


  /**
   * Returns a subset of Paginate::$set that should be displayed on the current page at Paginate::$offset.
   *
   * @return array
   */
  public function getResultsAtOffset()
  {
    $start = ($this->offset - 1) * $this->limit;
    $end   = $this->limit;

    return array_slice($this->set, $start, $end);
  }


  /**
   * Determines the total number of pages for the resultset.
   *
   * @return int
   */
  public function getTotalPages()
  {
    return ceil(count($this->set) / $this->limit);
  }


  /**
   * Returns the previous page value if there is one.
   *
   * @return int|boolean
   */
  public function previous()
  {
    if($this->offset > 1) {
      return ($this->offset - 1);
    } else {
      return false;
    }
  }


  /**
   * Returns the next page value if there is one.
   *
   * @return int|boolean
   */
  public function next()
  {
    if($this->total > $this->offset) {
      return ($this->offset + 1);
    } elseif($this->total <= $this->offset) {
      return false;
    }
  }
}