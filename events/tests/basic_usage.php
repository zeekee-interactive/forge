<?php

/**
 * Manual inclusion for the sake of testing; you should use Composer's autoloader over this.
 */
require_once __DIR__ . '/../zeekee/Event/Registry.php';
require_once __DIR__ . '/../zeekee/Event/Dispatcher.php';


/**
 * All the "test only" shit... you can ignore this.
 */

class Foo {
  public function bar() {
    return 'world!';
  }
}

$example_object = new Foo;

function say_hello()
{
  global $event;

  return 'Hello ' . $event->response;
}

function raise_exception()
{
  throw new Exception('Example of an exception raised during propagation.');
}

/**
 * The stuff you should be interested in!
 */

use Zeekee\Event\Registry;
use Zeekee\Event\Dispatcher;

$event = new Dispatcher(new Registry);

$event->registry->on('foo', array(
  array($example_object, 'bar'),
  'say_hello',
  'raise_exception'
));

// Produces "Hello world!", then raises an exception
$status = $event->call('foo');

if(!$status) {
  var_dump($event->error);
} else {
  print $event->response . PHP_EOL;
}