<?php

namespace Zeekee\Event;
use Zeekee\Event\Registry;
use Exception;

class Dispatcher
{
  /**
   * Stores the previous return value for the next callback.
   *
   * @access public
   * @var mixed
   */
  public $response;


  /**
   * References a caught exception in an event's propagation.
   *
   * @access public
   * @var object|null
   */
  public $error = null;


  /**
   * Injects the Registry class so there are events to work against.
   *
   * @param $registry object An instance of Zeekee\Event\Registry.
   * @return void
   */
  function __construct(Registry $registry)
  {
    $this->registry = $registry;
  }


  /**
   * "Calls" an event, causing the stack to propagate until an exception is raised or the end is reached.
   *
   * @param $name string The name of the event to call.
   * @param $arguments mixed An optional array of arguments, defaults to null.
   * @throws Exception if an unbound event was called.
   * @return boolean Only returns true if the operation was successful (that is to say, no exceptions were raised).
   */
  public function call($name, $arguments = null)
  {
    if(!$this->registry->has($name)) {
      throw new Exception('No event could be found in the Registry with the name ' . $name);
    }

    $stack = $this->registry->get($name);

    foreach($stack as $handler) {
      try {
        if(!is_null($arguments)) {
          $this->response = call_user_func_array($handler, $arguments);
        } else {
          $this->response = call_user_func($handler);
        }
      } catch(Exception $e) {
        $this->error = $e;

        return false;
      }
    }

    return true;
  }
} 