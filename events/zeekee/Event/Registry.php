<?php

namespace Zeekee\Event;
use Exception;

class Registry
{
  /**
   * A dictionary of event names to their callbacks.
   *
   * @access protected
   * @var array
   */
  protected $events = array();


  /**
   * Registers an event to the object.
   *
   * @param $name string Name of the event; does not need to be unique, in the case of a non-unique name, the $callbacks will be appended.
   * @param $callbacks mixed A function name, tuple containing an object and method to call, or a list of either.
   * @return void
   */
  public function on($name, $callbacks)
  {
    if(!$this->has($name)) {
      $this->events[$name] = array();
    }

    if(is_array($callbacks)) {
      $this->events[$name] += $callbacks;
    } else {
      $this->events[$name][] = $callbacks;
    }
  }


  /**
   * Tells us whether the event is bound to the registry.
   *
   * @return boolean
   */
  public function has($name)
  {
    return array_key_exists($name, $this->events);
  }


  /**
   * Returns the handler list for an event.
   *
   * @param $name string The event to search for.
   * @throws Exception if the event is not defined.
   * @return array
   */
  public function get($name)
  {
    if(!$this->has($name)) {
      throw new Exception('No event could be found in the Registry with the name ' . $name);
    }

    return $this->events[$name];
  }
}